# Documentation

## Table of Contents
1. [Configuration](#configuration)
   >* [Sublime Config](#sublime-config)
1. [Application Structure](#application-structure)
1. [State Tree Formation](#state-tree-formation)
1. [Notes](#notes)
1. [React Router](#react-router)
   >* [Path Syntax](#router-path)
   >* [Navigating between Routes](#navigating-between-routes)
1. [Import and Export](#import-export)
1. [Component Life Cycle](#component-life-cycle)
1. [Context Types](#context-types)
1. [Props Types](#props-types)
1. [Accessing refs](#accessing-refs)
1. [Handling Network Response](#handling-network-response)
1. [Using Objective Spread Operator](#objective-spread-operator)
1. [Possible Errors](#possible-errors)


## Application Structure
The application structure presented in this boilerplate is 

```
.
├── bin                      # Build/Start scripts
├── blueprints               # Blueprint files for redux-cli
├── build                    # All build-related configuration
│   └── webpack              # Environment-specific configuration files for webpack
├── config                   # Project configuration settings
├── server                   # Koa application (uses webpack middleware)
│   └── main.js              # Server application entry point
├── src                      # Application source code
│   ├── index.html           # Main HTML page container for app
│   ├── main.js              # Application bootstrap and rendering
│   ├── actions              # contains collection of actions and API Network calls for corresponding action
│   ├── components           # Reusable Presentational Components
│   ├── constants            # App specific constants
│   ├── containers           # Reusable Container Components
│   ├── layouts              # Components that dictate major page structure
│   ├── reducers             # structure of state tree & functions to transform previous state to next state based upon an action. 
│   ├── redux                # "Ducks" location...
│   │   └── modules          # reducer, action, creators not part of a route
│   ├── routes               # Main route definitions and async split points
│   │   ├── index.js         # returns route definition
│   ├── static               # Static assets (not imported anywhere in source code)
│   ├── store                # Redux-specific pieces
│   │   ├── createStore.js   # Create and instrument redux store
│   │   └── reducers.js      # Reducer registry and injection
│   └── styles               # Application-wide styles (generally settings)
│   ├── utils                # common utility functions .ex: generic web api call etc,
│   ├── views                # individual views for every route(URL),
├── tests                    # Unit tests
├── .eslintrc                # es linter configuration file for specifying rules etc., 
```

## State Tree Formation
* make readable & proper naming convention to state tree variables.
* include **isLoading**, **isValid** variables in the reducers for every API call.
* if API call response is success then make changes to these variables isLoading to **false** and **isValid** to true 

* **In components or views, we should render corresponding content based upon these flags only.**
  
```
#!java
    /* 
    * here searchObject keys are used to make a request . 
    * if API call response is success then make changes to these variables isLoading to false and isValid to true . 
    */

    const searchObject = {
      isLoading: true,
      isValid: false,
      searchString: '',
      category: 'All',
      subcategory: ''
    }
```

* categorize the state tree variables and create nested reducers like below.
* here searchObject and searchFilters reducers and grouped together as they are related to search.
* the main advantage with categorization is instead of sending whole state tree to components we can send minimal part of state tree which is required for a particular component.

```
#!java
    /* here searchObject and searchFilters reducers and grouped together as they are related to search */
    search: combineReducers({
      searchObject: searchReducer.searchReducer,
      searchFilters: searchReducer.searchFiltersReducer
    }),
```

#Notes

* Keep the render function as minimal as possible. make functions out side of render() for any modifications data etc., and call those functions inside the render().

* bind the required state variables and action creators in ``views`` and send them as props to child ``components``.

* all the network calls should be in ``actions`` and any modifications required in the server response should be done at reducers or at ``reducer helpers``

* reduce the too much usage of ``this.props`` in render() function. instead use es6 syntax to declare variables in the starting of render and use those variables in rest of the render function.


```
#!java

// bad
render () {
      return (
        <div >
          {this.props.categories.categoriesList}
          
          {this.props.categories.groupName}
       )
}


// ____good practice____ 
render () {
    let {categoriesList, groupName} = this.props.categories
      return (
        <div >
          {categoriesList}
          
          {groupName}
       )
}
```

## React Router

* ### Path Syntax
A route path is [a string pattern](/docs/Glossary.md#routepattern) that is used to match a URL (or a portion of one). Route paths are interpreted literally, except for the following special symbols:

  - `:paramName` – matches a URL segment up to the next `/`, `?`, or `#`. The matched string is called a [param](/docs/Glossary.md#params)
  - `()` – Wraps a portion of the URL that is optional
  - `*` – Matches all characters (non-greedy) up to the next character in the pattern, or to the end of the URL if there is none, and creates a `splat` [param](/docs/Glossary.md#params)
  - `**` - Matches all characters (greedy) until the next `/`, `?`, or `#` and creates a `splat` [param](/docs/Glossary.md#params)

```jsx
    <Route path="/hello/:name">         // matches /hello/michael and /hello/ryan
    <Route path="/hello(/:name)">       // matches /hello, /hello/michael, and /hello/ryan
    <Route path="/files/*.*">           // matches /files/hello.jpg and /files/hello.html
    <Route path="/**/*.jpg">            // matches /files/hello.jpg and /files/path/to/file.jpg
```

* ### Navigating between Routes

    * Do not use ``<Link />`` to switch between routes . use **``context.router``** always
 
    ### context.router

       * Contains data and methods relevant to routing. Most useful for imperatively transitioning around the application.
    
       * By default context.router will not be available to react class component. we have to mention explicitly about router as contextType outside of the class.

```
#!java

    var ExampleComponent = React.createClass({ ... });


    ExampleComponent.contextTypes = {
     router: React.PropTypes.object
    };


```

 **push(path Or Location)**

 router.push function is used to change the current route by specifying new route as path name

 Transitions to a new URL, adding a new entry in the browser history.


```
#!bash

    this.context.router.push({
      pathname: '/users/12'
    })

```
## Import and Export ##

Exporting without default means it's a "named export". You can have multiple named exports in a single file. So if you do this,
```
export class Template {}
export class AnotherTemplate {}
```
then you have to import these exports using their exact names. So to use these components in another file you'd have to do,
```
import {Template, AnotherTemplate} from './components/templates'
```
Alternatively if you export as the default export like this,
```
export default class Template {}
```
Then in another file you import the default export without using the {}, like this,
```
import Template from './components/templates'
```
There can only be one default export per file. In React it's a convention to export one component from a file, and to export it is as the default export.

You're free to rename the default export as you import it,
```
import TheTemplate from './components/templates'
```
And you can import default and named exports at the same time,
```
import Template,{AnotherTemplate} from './components/templates'
```

## Component Life Cycle
* Every React component should follow the below ordering   

* Ordering for `class extends React.Component`:

 
    1. optional `static` methods

    1. `constructor`

    1. `getChildContext`

    1. `componentWillMount`

    1. `componentDidMount`

    1. `componentWillReceiveProps`

    1. `shouldComponentUpdate`

    1. `componentWillUpdate`

    1. `componentDidUpdate`

    1. `componentWillUnmount`

    1. *clickHandlers or eventHandlers* like `onClickSubmit()` or `onChangeDescription()`

    1. `render` function

## [Context Types](https://www.tildedave.com/2014/11/15/introduction-to-contexts-in-react-js.html)

* In React.js a **context** is a set of attributes that are implicitly passed down from an element to all of its children and grandchildren.

* simpley **context** is a way to pass values through a tree without having to use props at every single point.

* Any element that wants to access a variable in the context must explicitly a contextTypes property. If this is not declared, it will not be defined in the elements this.context variable (and you will likely have errors!).

* If you specify a context for an element and that element renders its own children, those children also have access to the context (whether or not their parents specified a contextTypes property).

* Child contexts allow an element to specify a context that applies to all of its children and grandchildren. This is done through the childContextTypes and getChildContext properties.

Example :
```
#!java

    var A = React.createClass({

        childContextTypes: {
             name: React.PropTypes.string.isRequired
        },

        getChildContext: function() {
             return { name: "Jonas" };
        },

        render: function() {
             return <B />;
        }
    });

    var B = React.createClass({

        contextTypes: {
            name: React.PropTypes.string.isRequired
        },

        render: function() {
            return <div>My name is: {this.context.name}</div>;
        }
    });

    // Outputs: "My name is: Jonas"
    React.render(<A />, document.body);
```

## [Prop Types](https://facebook.github.io/react/docs/reusable-components.html)

* We use several reusable components, Inorder to ensure that they are receiving valid props everytime, we need to specify propTypes for that component.

* ``React.PropTypes`` exports a range of validators that can be used to make sure the data you receive is valid. When an invalid value is provided for a prop, a warning will be shown in the JavaScript console. 

* as similar to contextTypes these propTypes should also be mentioned outside of class. 

```
#!java
export default class ExampleComponent extends Component { render() { ... } }

ExampleComponent.propTypes = {
 aStringProp: React.PropTypes.string,
 numberProp: React.PropTypes.number.isRequired
};

ExampleComponent.defaultProps = {
 aStringProp: ''
};


```

* other way of binding propTypes and contextTypes with in the class is 


```
#!java
export default class ExampleComponent extends Component { 
   static propTypes = {
     aStringProp: React.PropTypes.string,
     numberProp: React.PropTypes.number.isRequired
   };
 
   static contextTypes = {
     router: React.PropTypes.object
   };

   render() { ... } 
  }

```

### Accessing ref's
* you must access using ``this.refs['myRefString']`` if your ref was defined as ref="myRefString".

* to get value of input box or etc., use ``ReactDOM.findDOMNode(this.refs['myRefString']).value``

## Handling Network Response ##

If the network response is nested, you have to use multiple maps to get whole data. Don't write nested maps in `render()`. Create components for each map. Otherwise we can use normalizr to flatten the nested data.

## Using Object Spread Operator ##

Since one of the core tenets of Redux is to never mutate state, we’ll often find ourself using `Object.assign()` to create copies of objects with new or updated values.
```
export function productReducer (state = selectedProductObject, action) {
  switch (action.type) {
    case 'GET_PRODUCT_INFO':
      return (
        Object.assign({},state, 
          {selectedProduct: action.response}
        )
      )
    default:
      return state
  }
}
```
While effective, using `Object.assign()` can quickly make simple reducers difficult to read given its rather verbose syntax.

An alternative approach is to use the object spread syntax proposed for the next versions of JavaScript which lets you use the spread `(...)` operator to copy enumerable properties from one object to another in a more succinct way.
```
export function productReducer (state = selectedProductObject, action) {
  switch (action.type) {
    case 'GET_PRODUCT_INFO':
      return {...state, selectedProduct: action.response}
    default:
      return state
  }
}
``` 

# Possible Errors occurs during Development

* Warning: Each child in an array or iterator should have a unique "key" prop.

   **sol:** add unique index as a value of prop name `key` to return() function inside the map 
   

```
#!java

object.map(function (category, index) {
      return (
        <div key={index}>
       ... 
       </div>
```

* Warning: React.createElement: type should not be null, undefined, boolean, or number. It should be a string (for DOM elements) or a ReactClass (for composite components). Check the render method

The warning is because of the following mistake
```
export default class SubCategory extends component{}
```
If the component SubCategory is exported as
```
import SubCategory from './components'
```
**sol:** check import statements in the file given in warning

The SubCategory should be imported like
```
import {SubCategory} from './components'
```