import React from 'react'
import {Route, IndexRoute} from 'react-router'
import CoreLayout from '../layouts/CoreLayout/CoreLayout'
import HomeView from '../views/HomeView.react'
// bind the view components to appropriate URL path
export default (store) => (
  <div>
    <Route path='/' component={CoreLayout} >
      <IndexRoute
        component={HomeView}
      />
      <Route
        path='home'
        component={HomeView}
      />
    </Route>
  </div>

)
