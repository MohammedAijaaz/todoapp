import React, { Component} from 'react'
import {Link} from 'react-router'
import {HelloWorld} from './HelloWorld.react.js'

console.log('HelloWorld', HelloWorld)
export default class HomeView extends Component {
  render () {
    return (
      <div className='container'>
        <div className='container'>
          <div className='jumbotron'>
            <h1>Welcome to React Tutorial</h1>
            <p>Let's Start </p>
          </div>
        </div>
        <HelloWorld />
      </div>
    )
  }
}
