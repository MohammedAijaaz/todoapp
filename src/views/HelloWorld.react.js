// An example with es5 Syntax
var React = require('react')

var HelloWorld = React.createClass({
  render: function () {
    return (
      <div className='container'>
        <h2>
          Hello World!
        </h2>
      </div>
    )
  }
})

exports.HelloWorld = HelloWorld
