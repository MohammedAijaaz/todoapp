// step 1: define the `api_name`  with corresponding 'url' to be called
// the success response of api will be triggered as `SET__API_NAME__INFO` action creator
var apis = {
  userLogin: {api: 'user/login/', method: 'POST'}
}

export function getApi (endPoint) {
  // console.log(endPoint, apis[endPoint])
  return apis[endPoint] || {api: endPoint, method: 'POST'}
}
