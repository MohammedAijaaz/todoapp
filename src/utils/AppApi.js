import {getAccessToken} from 'utils/AppUtils'
import {BASE_URL} from './baseURL'

var url
var requestObject
var onSuccess
var onFailure
var type
var count
var maxRetries = 2

// generic web api call to make network requests
function genericWebAPICall (url, requestObject, onSuccess, onFailure, type = 'POST', count = 1, choice = false) {
  url = url
  // console.log(url)
  requestObject = requestObject
  onSuccess = onSuccess
  onFailure = onFailure
  type = type
  console.log(url)
  if (count < 2) {
    console.log('RequestObject:', requestObject)
    if (type === 'POST') {
      const formattedrequestObject = JSON.stringify(JSON.stringify(requestObject))
      const dataRequestObject = JSON.stringify({'data': formattedrequestObject, 'clientKeyDetailsId': 1})
      console.log('dataRequestObject >>> ', dataRequestObject)
      // let accessToken = getAccessToken()
      let accessToken = 'todo_app'
      $.ajax({
        type: 'POST',
        url: url,
        data: dataRequestObject,
        contentType: 'application/json',
        beforeSend: function (xhr) {
          if (accessToken !== '' || accessToken !== null) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + accessToken)
          }
        }
      })
      .done(function (response) {
        // console.log(response)
        onSuccess(response)
      })
      .fail(function (response) {
        // console.log(response)
        if (count === 1) {
          onFailure(response)
        } else {
          genericWebAPICall(url, requestObject, onSuccess, onFailure, type, count + 1)
        }
      })
    } else if (type === 'GET') {
      // let accessToken = getAccessToken()
      let accessToken = 'todo_app'
      $.ajax({
        type: 'GET',
        url: url,
        data: requestObject,
        contentType: 'application/json',
        beforeSend: function (xhr) {
          if (accessToken !== '' || accessToken !== null) {
            xhr.setRequestHeader('Authorization', 'Bearer ' + accessToken)
          }
        }
      })
      .done(onSuccess)
      .fail(function (response) {
        if (count === 1) {
          onFailure(response)
        } else {
          genericWebAPICall(url, requestObject, onSuccess, onFailure, type, count + 1)
        }
      })
    }
  }
}

export {
  genericWebAPICall
}
