import React, {Component} from 'react'
import {Link} from 'react-router'
import {logo} from '../../static/assets/'

export default class Header extends Component {
  constructor (props, context) {
    super(props, context)
    this.state = { showModal: false }
  }

  close = () => {
    this.setState({ showModal: false })
  }

  open = () => {
    this.setState({ showModal: true })
  }
  render () {
    console.log('Hai')
    return (
      <nav className='navbar navbar-toggleable-md navbar-light bg-faded'>
        <button className='navbar-toggler navbar-toggler-right' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
          <span className='navbar-toggler-icon' />
        </button>
        <a className='navbar-brand' href='https://www.ibhubs.co' target='_blank'>
          <img
            style={{width: '30px'}}
            className='img img-responsive'
            alt='iB Hubs!'
            src={logo}
          />
        </a>

        <div className='collapse navbar-collapse' id='navbarSupportedContent'>
          <ul className='navbar-nav mr-auto'>
            <li className='nav-item active'>
              <Link className='nav-link' to='/home'>
                Home
                <span className='sr-only'>(current)</span>
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    )
  }
}
